import 'package:flutter/material.dart';
import 'package:newsapp/Models/articlesModel.dart';
import 'package:newsapp/view%20Model/list%20of%20article%20view.dart';
import 'package:newsapp/views/view.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(NewsApp());
}

class NewsApp extends StatelessWidget {

  ArticlesList articlesList = ArticlesList();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ArticleListViewModel>(
        create: (context) => ArticleListViewModel(),
        child: MaterialApp(
            home: HomeView()
        ))
    ;
  }
}

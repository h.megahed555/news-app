class Article {
 final String  title ;
 final String  description ;
 final String  imageUrl ;
 final String  articlesUrl ;

  Article({this.title, this.description, this.imageUrl, this.articlesUrl});

  factory Article.fromJson(Map<String , dynamic> jsonData ){
   return Article(
    title: jsonData['title'],
    articlesUrl: jsonData['url'],
    description: jsonData['description'],
    imageUrl: jsonData['urlToImage']
   );
  }



}
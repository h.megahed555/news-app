import 'package:flutter/material.dart';
import 'package:newsapp/view%20Model/list%20of%20article%20view.dart';
import 'package:provider/provider.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: () async {
            await Provider.of<ArticleListViewModel>(context, listen: false)
                .fetchArticle();
            print(Provider.of<ArticleListViewModel>(context, listen: false)
                .articleList);
          },
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:newsapp/Models/article_model.dart';
import 'package:http/http.dart' as http;
import 'package:newsapp/Models/articlesModel.dart';


  class NewsApi {
  final String apiKey = '2006ad2f17b747079eddfb1974546885';
  Future<List<Article>> fetchArticles() async {
  try {
  http.Response response = await http.get(
  'https://newsapi.org/v2/top-headlines?country=eg&apiKey=$apiKey');
  if (response.statusCode == 200) {
  String data = response.body;
  var jsonData = jsonDecode(data);
  //converted to json but data still unknown
  ArticlesList articles = ArticlesList.fromJson(jsonData);
  List<Article> articlesList =
  articles.articles.map((e) => Article.fromJson(e)).toList();
  return articlesList;
    //each article in articles stored in articlesList
  } else {
  print('status code = ${response.statusCode}');
  }
  } catch (ex) {
  print(ex);
  }
  }
  Future<List<Article>> fetchArticlesByCategory(String category) async {

    try {
      http.Response response = await http.get(
          'https://newsapi.org/v2/top-headlines?country=eg&category=$category&apiKey=$apiKey');
      if (response.statusCode == 200) {
        String data = response.body;
        var jsonData = jsonDecode(data);
        //converted to json but data still unknown
        ArticlesList articles = ArticlesList.fromJson(jsonData);
        List<Article> articlesList =
        articles.articles.map((e) => Article.fromJson(e)).toList();
        return articlesList;
        //each article in articles stored in articlesList
      } else {
        print('status code = ${response.statusCode}');
      }
    } catch (ex) {
      print(ex);
    }
  }
  }




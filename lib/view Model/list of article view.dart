import 'package:flutter/cupertino.dart';
import 'package:newsapp/Models/article_model.dart';
import 'package:newsapp/services/news_services.dart';

class ArticleListViewModel extends ChangeNotifier {
  List<Article> _articleList = [];
  List<Article> _articleListByCategory = [];
  Future<void> fetchArticle() async {
    _articleList = await NewsApi().fetchArticles();
    notifyListeners();
  }

  Future<void> fetchArticleByCategory(String category) async {
    _articleListByCategory = await NewsApi().fetchArticlesByCategory(category);
    notifyListeners();
  }

  List<Article> get articleList => _articleList;
  List<Article> get articleListByCategory => _articleListByCategory;
}
